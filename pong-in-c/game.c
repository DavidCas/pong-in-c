#include<stdio.h>
#include<stdlib.h>
#include<X11/keysym.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<time.h>
#include<math.h>
#include "my_delay.h"
#include "ball.h"
#include "paddle.h"
#include "environment.h"
#include "my_numbers.h"
#include "score_counter.h"
#include "effects.h"

#ifndef TRUE_FALSE 
#define TRUE 1
#define FALSE 0
#endif

struct Number_buffer {
    int number;
    int x_position;
    int y_position;
    int digits;
};

struct Number_shake_wrapper {
    struct Y_Shake_Animation * shake;
    struct Number_buffer * number;
};

int * win_width;
int * win_height;
int win_half_width;
int win_half_height;
float frametime;
Paddle * left_paddle_ptr;
Paddle * right_paddle_ptr;
Ball * ball_ptr;
Border top_border;
Border bottom_border;
Border * top_border_ptr;
Border * bottom_border_ptr;
Score * player_one_score_ptr;
Score * player_two_score_ptr;
struct Number_buffer * player_one_number_buffer;
struct Number_buffer * player_two_number_buffer;
struct Explosion * explosion = NULL;
struct Shake_Animation * shake = NULL;
struct Number_shake_wrapper * shake_wrapper;
struct Number_buffer * set_number_buffer(struct Number_buffer *buffer, Score * score);

void eventloop(Window * win, Display * display, int * screen);
void process_event(Window * win, Display * display, int * screen, XEvent report);
void draw_graphics(Window * win, Display * display, int * screen);
void draw_grid(Window * win, Display * display, GC*);
bool is_goal_for_player_one(Ball * ball, int * win_width);
bool is_goal_for_player_two(Ball * ball);
void draw_score(
    Display * display, 
    Window * win, 
    GC * gc, 
    struct Number_buffer * buffer);
void handle_ball_movement(
    Window * win, 
    Display * display, 
    int * screen);
void handle_shake(
    Window * win,
    Display * display,
    int * screen
);
void handle_explosion(
    Window * win,
    Display * display,
    int * screen
);

void simulate_game(Window * win, Display * display , int * screen, int * width, int * height) {
    int quit = FALSE;
    frametime = (float)FRAME_LEN_60;
    int td;
    int pause = 0;
    win_width = width;
    win_height = height;
    win_half_width = (*win_width) / 2;
    win_half_height = (*win_height) / 2;
    int player_one_score_x_pos = (int) (*win_width/4 - 15);
    int player_two_score_x_pos = (int) (*win_width * 0.75 - 15);
    player_one_number_buffer = calloc(1, sizeof(struct Number_buffer));
    player_two_number_buffer = calloc(1, sizeof(struct Number_buffer));

    top_border = (Border){0, 0, *win_width, 10};
    bottom_border = (Border){0, *win_height-10, *win_width, 10};
    top_border_ptr = &top_border;
    bottom_border_ptr = &bottom_border;
    left_paddle_ptr = initialize_paddle(10, 100, 10, 100);
    right_paddle_ptr = initialize_paddle(*win_width-20, 100, 10, 100);
    //initialize_paddle(right_paddle_ptr, *win_width-20, 20, 10, *win_height-40);
    ball_ptr = initialize_ball(win_half_width, win_half_height, 10, 10, &frametime);
    player_one_score_ptr = initialize_score(player_one_score_x_pos, 20, win_width, win_height);
    set_number_buffer(player_one_number_buffer, player_one_score_ptr);
    player_two_score_ptr = initialize_score(player_two_score_x_pos, 20, win_width, win_height);
    set_number_buffer(player_two_number_buffer, player_two_score_ptr);    
    while(!quit) {
        td = time_diff();
        pause = delay(frametime - td + pause);
        eventloop(win, display, screen);
    }
}

void eventloop(Window * win, Display * display, int * screen) {
    XEvent xev; //Events like pressed key's
    int num_events;

    XFlush(display);

    num_events =  XPending(display);

    handle_ball_movement(win, display, screen);
    handle_shake(win, display, screen);
    handle_explosion(win, display, screen);

    while ((num_events != 0)) {
        num_events--;
        XNextEvent(display, &xev);
        process_event(win, display, screen, xev);
    }
}

void process_event(Window * win, Display * display, int * screen, XEvent report) {
    KeySym key;
    int keycode;
    int bufsize = 128;
    char buf[bufsize];
    int has_image_changed = FALSE;

    switch (report.type) {

        /*case Expose:
        if(report.xexpose.count == 0) {
            has_image_changed = TRUE;
        }

        break;*/

        case KeyPress:
        key = XLookupKeysym(&report.xkey, 0);
        keycode = XKeysymToKeycode(display, key);
        XLookupString(&report.xkey, buf, bufsize, &key, NULL);

        /*
        if(strlen(buf) != 0 && buf[0] != '\n' && buf[0] != '\r') {
            printf("Taste '%s' (Tastaturcode: %d) wurde gedrückt.\n",
            buf, XKeysymToKeycode(display, key));
        }
        else
        {
            printf("Taste ’%s’ (Tastaturcode: %d) wurde gedrückt.\n",
            XKeysymToString(key), XKeysymToKeycode(display, key));
        }
        */
        

        switch (keycode)
        {
            case 38:
                move_paddle_up(left_paddle_ptr);
                has_image_changed = TRUE;
                break;

            case 52:
                move_paddle_down(left_paddle_ptr, win_height);
                has_image_changed = TRUE;
                break;

            case 111:
                move_paddle_up(right_paddle_ptr);
                has_image_changed = TRUE;
                break;
        
            case 116:
                move_paddle_down(right_paddle_ptr, win_height);
                has_image_changed = TRUE;
                break;
        
            default:
                break;
        }
        
        break;

        case KeyRelease:
        //printf("Taste wieder losgelassen.\n");
        break;

        case ButtonPressMask:
        //printf("Maus-Event\n");
        break;

        default:
        break;
    }

    if(has_image_changed == TRUE) {
        draw_graphics(win, display, screen);
    }
}

void draw_graphics(Window * win, Display * display, int * screen) {
    GC mygc;
    long black, white;

    //Set constants for colors
    black = BlackPixel(display, *screen);
    white = WhitePixel(display, *screen);

    mygc = XCreateGC(display, *win, 0, 0);
    XSetForeground(display, mygc, black);
    XSetBackground(display, mygc, white);

    XClearWindow(display, *win);

    //draw_grid(win, display, &mygc);

    draw_border(display, win, &mygc, win_width, win_height);
    draw_center_line(display, win, &mygc, win_width, win_height);
    draw_paddle(display, win, &mygc, left_paddle_ptr);
    draw_paddle(display, win, &mygc, right_paddle_ptr);
    draw_ball(display, win, &mygc, ball_ptr);
    draw_score(display, win, &mygc, player_one_number_buffer);
    draw_score(display, win, &mygc, player_two_number_buffer);

    if (explosion != NULL) {
        if (explosion->is_in_progress) {
        draw_explosion(display, win, &mygc, explosion);
        }
    }

    if (shake != NULL) {
        
    }
    

    XFreeGC(display, mygc);
}

void draw_score(
    Display * display, 
    Window * win, 
    GC * gc, 
    struct Number_buffer * number_buffer) {

        for (int i = 0; i < number_buffer->digits; i++)
        {
            struct Number_buffer * current_buffer = number_buffer+i;
            draw_number(display, win, gc, current_buffer->x_position, current_buffer->y_position, current_buffer->number);
        }
}

/**
 * For debugging purpose for example. Draws a grid with 10x10 pixel size per cell.
 * */
void draw_grid(Window * win, Display * display, GC * mygc) {
    XDrawRectangle(display, *win, *mygc, 0, 0, *win_width-1, *win_height-1);

    /*Draw horizontal lines*/
    int y_position = 9;

    for(y_position; y_position <= *win_height-1; y_position += 10) {
        XDrawLine(display, *win, *mygc, 0, y_position, *win_width-1, y_position);
    }

    /*Draw vertical lines*/
    int x_position = 9;

    for (x_position; x_position <= *win_width-1; x_position += 10) {
        XDrawLine(display, *win, *mygc, x_position, 0, x_position, *win_height);
    }
}

void handle_ball_movement(Window * win, Display * display, int * screen) {
    if (has_ball_moved(ball_ptr, &frametime, win_width, left_paddle_ptr, right_paddle_ptr, top_border_ptr, bottom_border_ptr) == 1) {
        draw_graphics(win, display, screen);

        //printf("Ball x pos goal player 1: %d.\n", ball_ptr->x_position);
        //printf("Ball x pos goal player 1: %f.\n", ball_ptr->x_pos_internal);

        if (is_goal_for_player_one(ball_ptr, win_width))
        {
            //explosion = instanciate_Explosion(&ball_ptr->x_position, &ball_ptr->y_position, 8, 10);
            //shake = instanciate_Shake(player_two_score_ptr->y_position);
            //struct Number_shake_wrapper * shake_wrapper = malloc(sizeof(struct Number_shake_wrapper));
            //shake_wrapper->number = player_two_number_buffer;
            //shake_wrapper->shake = shake;
            increase_score(player_one_score_ptr);
            player_one_number_buffer = set_number_buffer(player_one_number_buffer, player_one_score_ptr);
            change_ball_position(ball_ptr, &win_half_width, &win_half_height);
        }

        if(is_goal_for_player_two(ball_ptr)) {
            //explosion = instanciate_Explosion(&ball_ptr->x_position, &ball_ptr->y_position, 8, 100);
            //explosion = instanciate_Explosion(&win_half_width, &win_half_height, 8, 100);
            //shake = instanciate_Shake(player_one_score_ptr->y_position);
            increase_score(player_two_score_ptr);
            player_two_number_buffer = set_number_buffer(player_two_number_buffer, player_two_score_ptr);
            change_ball_position(ball_ptr, &win_half_width, &win_half_height);
        }
    }
}

void handle_shake(Window * win, Display * display, int * screen) {
    if (shake != NULL) {
        if (shake->is_in_progress) {
            shake_wrapper->number->y_position = shake_y_pos(shake, shake_wrapper->number->y_position);
            draw_graphics(win, display, screen);
        }
        else
        {
            free(shake);
            shake = NULL;
        }
    }
}

void handle_explosion(Window * win, Display * display, int * screen) {
    /*Right now, number of explosion is fixed. Will be fixed to get independent numbers of explosions dynamically.*/
    if (explosion != NULL) {
        if (explosion->is_in_progress) {
        expand_explosion_the_noob_way(explosion);
        draw_graphics(win, display, screen);
        }
        else {
            free(explosion);
            explosion = NULL;
        }
    }
}

bool is_goal_for_player_one(Ball * ball, int * win_width) {
    return ball->x_position >= *win_width - ball->width ? true : false;
}

bool is_goal_for_player_two(Ball * ball) {
    return ball->x_position <= 0 ? true : false;
}

struct Number_buffer * set_number_buffer(struct Number_buffer *buffers, Score * score) 
{
    int score_digits = score->score == 0 ? 1 : (int) (log10(score->score) + 1);
    bool is_digits_value_odd = (score_digits % 2) == 0 ? false : true;
    int shift_to_one_side = 40 * (score_digits / 2);
    int number_buffer = score->score;
    int x_pos_buffer = score->x_position;
    int digits_buffer = score_digits;

    buffers = realloc(buffers, score_digits * sizeof(struct Number_buffer));

    if (!is_digits_value_odd) {
        x_pos_buffer = x_pos_buffer + 20 + (40 * ((score_digits / 2) - 1));
    }

    else
    {
        x_pos_buffer = x_pos_buffer + 40 * (score_digits / 2);
    }

    buffers->digits = score_digits;

    //Set every buffer
    for (int i = 0; i < score_digits; i++)
    {
        (buffers+i)->x_position = x_pos_buffer;

        (buffers+i)->number = number_buffer % 10;
        (buffers+i)->digits = digits_buffer;
        (buffers+i)->y_position = score->y_position;
        x_pos_buffer -= 40;
        number_buffer /= 10;
        digits_buffer -= 1;
    }

    return buffers;
};

void set_y_pos_for_score_to_shake(struct Shake_Animation * shake, struct Number_buffer * number) {
    number->y_position = shake_y_pos(shake, number->y_position);
}