#include<X11/Xlib.h>
#include<X11/Xutil.h>

#ifndef ENVIRONMENT_HEADER
#define ENVIRONMENT_HEADER
typedef struct
{
    int x_position;
    int y_position;
    int width;
    int height;
}Border;


void draw_border(Display *display, Window *win, GC *gc, int * window_width, int * window_height);
void draw_center_line(Display *display, Window *win, GC *gc, int * window_width, int * window_height);
#endif