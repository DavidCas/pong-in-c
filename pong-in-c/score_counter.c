#include "score_counter.h"
#include <stdlib.h>
#include <math.h>

Score * initialize_score(
    int x_position, 
    int y_position,
    int * win_width, 
    int * win_height) 
{
    Score * score_counter = malloc(sizeof(Score));

    score_counter->score = 0;
    score_counter->x_position = x_position;
    score_counter->y_position = y_position;
    score_counter->win_width = win_width;
    score_counter->win_height = win_height;

    return score_counter;
}

/*
void set_score_list(Score_linked_list_type * score_linked_list) {

    // Check if to stop recursion
    if(((int) (score_linked_list->score/10)) == 0) {
        score_linked_list = NULL;
        return;
    }

    score_linked_list->right_number = score_linked_list->score % 10;

    int digits = (int) log10(score_linked_list->score);
    int factor_to_remove_first_digit = pow(10, digits);

    score_linked_list->left_number = 
        (int)(score_linked_list->score / factor_to_remove_first_digit);

    int score_without_first_digit = 
        score_linked_list->score % (int) factor_to_remove_first_digit;

    int score_without_last_digit = 
        (int) score_without_first_digit / 10;

    score_linked_list->score = score_without_last_digit;

    set_score_list(score_linked_list->next);
}

Score_Wrapper_Type * initialize_score_wrapper(
    Score_Wrapper_Type * score_wrapper,
    int x_position,
    int y_position,
    int * win_width,
    int * win_height
) 
{
    Score_linked_list_type * score_list = malloc(sizeof(Score_linked_list_type));

    Score * score = initialize_score(x_position, y_position, win_width, win_height);

    score_list->score = score->score;
    set_score_list(score_list);

    score_wrapper->score = score;
    score_wrapper->linked_list = score_list;
}
*/
int get_score(Score_Wrapper_Type * score_wrapper) {
    return score_wrapper->score->score;
}

void increase_score(Score * score_to_increase) {

    /*
    if(score_to_increase->score == 99) {
        score_to_increase->score = 0;
        
        return;
    }*/

    score_to_increase->score += 1;
}
/*
void increase_score_wrapper(Score_Wrapper_Type * score_to_increase) {
    score_to_increase->score->score += 1;
    score_to_increase->linked_list->score = score_to_increase->score->score;

    set_score_list(score_to_increase->linked_list);
}
*/

void reset_score(Score * score_to_reset) {
    score_to_reset->score = 0;
}

void change_score_position(Score * score_to_update, int x_pos, int y_pos) {
    score_to_update->x_position = x_pos;
    score_to_update->y_position = y_pos;
}