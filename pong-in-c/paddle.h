#include<X11/Xlib.h>
#include<X11/Xutil.h>

#ifndef PADDLE
#define PADDLE
typedef struct
{
    int x_position;
    int y_position;
    int width;
    int height;
}Paddle;
Paddle * initialize_paddle(int x_position, int y_position, int width, int height);
void move_paddle_up(Paddle * paddle_to_move);
void move_paddle_down(Paddle * paddle_to_move, int * win_height);
void draw_paddle(Display *display, Window *win, GC *gc, Paddle *paddle_to_draw);
#endif