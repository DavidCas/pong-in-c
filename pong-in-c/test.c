#include <stdio.h>
#include <math.h>
#include "game.c"

int main(int argc, char const *argv[])
{
    int x_position = 100;
    int number = 12345;
    int digits = (int) (log10(number) + 1);
    int current_x_position = x_position;
    int current_number = number;

    if ((digits % 2) == 0) {
        current_x_position = x_position + 20 + (40 * ((digits / 2) - 1));
        printf("%d\n", current_x_position);
    }

    else {
        current_x_position = x_position + 40 * ((digits / 2));
        printf("%d\n", current_x_position);
    }

    for (int i = 1; i <= digits; i++)
    {
        int single_number =  current_number % 10;
        current_number /= 10;
        printf("%d\n", single_number);
    }
    
    /*
    clock_t now = clock();
    clock_t after_now = clock();
    printf("Jetzt %ld und jetzt %ld.\n", now, after_now);
    //CLOCKS_PER_SEC ist eine Konstante aus der time.h Library,
    //welche irgendwie die Zahl der Ticks per Sekunden beinhaltet.
    printf("Ticks pro Sekunde: %ld.\n", CLOCKS_PER_SEC);

    printf("Messe 10 Sekunden.\n");

    long CLOCKS_PER_TEN_SEC = CLOCKS_PER_SEC * 10;
    now = clock();
    clock_t time_diff = clock() - now;

    printf("START\n");
    while (time_diff<CLOCKS_PER_TEN_SEC)
    {
        time_diff = clock() - now;
        printf("%ld\n", clock());
    }
    printf("STOP\n");
    printf("Ticks in 10 Sekunden ist %ld. Ticks laut Library %ld.\n", time_diff, CLOCKS_PER_TEN_SEC);*/
    return 0;
}
