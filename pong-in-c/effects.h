#include <stdbool.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>

struct Explosion
{
    float explosion_expansion;
    int x_position;
    int y_position;
    int max_radius;
    int number_of_particles;
    struct Explosion_Particle * particles;
    bool is_in_progress;
};

struct Explosion_Particle {
    float x_position;
    float y_position;
};

struct Shake_Animation {
    bool is_in_progress;
    float amplitude;
    float next_max_amplitude;
    float amplitude_end_value;
    float old_y_pos;
};

struct Explosion * instanciate_Explosion(int * x_position, int * y_position, int number_of_particles, int max_radius);
void instanciate_particles(struct Explosion_Particle * particles, struct Explosion * explosion);
bool is_explosion_in_progress(struct Explosion * explosion);
void expand(struct Explosion * explosion);
void explode(struct Explosion * explosion);
void expand_explosion_the_noob_way(struct Explosion * explosion);
void draw_explosion(Display * display, Window * win, GC * gc, struct Explosion * explosion);
struct Y_Shake_Animation * instanciate_Shake(float max_amplitude);
float shake_y_pos(struct Shake_Animation * shake, float y_position);
