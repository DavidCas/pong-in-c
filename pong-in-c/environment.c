#include "environment.h"
#include<X11/Xlib.h>
#include<X11/Xutil.h>

void draw_border(Display *display, Window *win, GC *gc, int * window_width, int * window_height) {
    //Draw top border
    XFillRectangle(display, *win, *gc, 0, 0, *window_width, 10);

    //Draw bottom border
    XFillRectangle(display, *win, *gc, 0, (*window_height-10), *window_width, 10);
}

void draw_center_line(Display *display, Window *win, GC *gc, int * window_width, int * window_height) {
    long center = *window_width/2;

    for (int i = 20; i <= (*window_height - 20); i += 20) {
        XFillRectangle(display, *win, *gc, (center - 5), i, 2, 10);
    }
}