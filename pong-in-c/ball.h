#include<stdlib.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<stdbool.h>
#include "paddle.h"
#include "environment.h"

#ifndef BALL
#define BALL
typedef struct
{
    int x_position;
    int y_position;
    int width;
    int height;
    float velocity_x;
    float velocity_y;
    float x_pos_internal;
    float y_pos_internal;
    bool are_internals_initialized;
}Ball;

Ball * initialize_ball(int x_position, int y_position, int width, int height, float * frametime);
int move_ball(
    Ball * ball_to_move, 
    float * frametime,
    int * win_width, 
    Paddle * left_paddle, 
    Paddle * right_paddle, 
    Border * top_border,
    Border * bottom_border);
void draw_ball(Display *display, Window *win, GC *gc, Ball *ball);
void change_ball_position(Ball *ball, int * new_x_pos, int * new_y_pos);
bool is_collision_with_paddle(Ball *ball, Paddle *left_paddle, Paddle *right_paddle);
bool is_collision_with_border(Ball *ball, Border *top_border, Border *bottom_border);
bool is_ball_out_of_bounds(Ball *ball, int win_width);
bool has_ball_moved(
    Ball * ball_to_check, 
    float * frametime, 
    int * win_half_width, 
    Paddle * left_paddle, 
    Paddle * right_paddle, 
    Border * top_border,
    Border * bottom_border);
#endif