# Pong in C

![](Videos/07_01_demo.mp4)

This is a recreation of the classic game Pong. It's implemented in the C
programming language and made for Linux OS. To display graphics on a window,
X library was used.

To compile the game, use the terminal and navigate to the folder
.../breakout/pong-in-c. <br/> 
Type in the console: <br/>
<code>bash build.sh</code>

Run the game with: <br/>
<code>./linux_platform</code>

## Controls
#### Left player:
A for up and Y for down
#### Right player:
Arrow keys for movement

#### Current state of development:

- Display a window under Linux.
- Draw some graphics inside a window.
- Draw paddles.
- Handle framerate.
- Define structure and functions for the paddles.
- Animation of paddles depended on user input.
- Define structure and functions for the ball.
- Draw a moving ball, independend on user inputs.
- Implemented more structures like borders.
- Implemented collision detection with playfield border and paddles.
- Move ball diagonally.
- Two handed pong version implemented.
- Define how every number from 0 to 9 has to be drawn.
- Define Score and functions to draw the score.
- Score counter implemented. Score position is always centralised automatically,
no matter how many digits the number has.

#### Planned upcomming features:

- Implementing effects like shaking effect for score for example and explosion.
- Implementing an AI, to fight against the computer.
  - Find and implement a simple idea for the AI, implemented manually.
  - After that, implement an AI with Deep Learning.
- Implement a start sequence like start to move the ball after pressing a 
specific button.
- Start screen is not planned, to keep every border away from the player to
start a game.
- Implement simple sounds.
- Port to windows.

#### Bugs that are planned to get fixed

- Flickering bug
- Fix collision bug, when the ball is "inside" the paddle.
- Paddles movemend delay, when a direction button is pressed continuously.
- Refacture source code to make the game portable to other OS's. Actually idea
was to make implement the game as a service for the OS platform like the 
linux platform. While development, this idea got out of the target.

## Current attempt to implement explosion animations
Here is my first attempt to render an explosion. All particles right now are
fixed in their positions, relative to a place of origin. Also the number of paricles. The idea is to calculate
the positions in runtime, dependent on the desired number of particles.

![](Videos/explosion_attempt.mp4)

Another problem is, that every particles is moving with a different velocity.
The idea was, that every particle moves with the same velocity. And every
every particle has the same distance to its two neighbor particles. Here are
current thoughts on the implementation as scribble.

![](Screenshots/scribble_explosion_pong.jpg)

The explosion will be used to render a ball explosion, when the ball hits a
goal. But the plan is to change the idea a bit. Because right now, half of the
explosion rendering would also be useless, because the ball explodes at the border
of the window.
