#ifndef SCORE_COUNTER_HEADER
#define SCORE_COUNTER_HEADER

typedef struct
{
    int score;
    int x_position;
    int y_position;
    int * win_width;
    int * win_height;
}Score;

typedef struct Score_linked_list
{
    int left_number;
    int right_number;
    int pixel_shift;
    int score;
    struct Score_linked_list * next;
}Score_linked_list_type;

typedef struct Score_Wrapper {
    Score * score;
    Score_linked_list_type * linked_list;
}Score_Wrapper_Type;


Score * initialize_score( 
    int x_position, 
    int y_position, 
    int * win_width, 
    int * win_height);
int get_score(Score_Wrapper_Type * score);
void increase_score(Score * score_to_increase);
void increase_score_wrapper(Score_Wrapper_Type * score_to_increase);
void reset_score(Score * score_to_reset);
void change_score_position(Score * score_to_update, int x_pos, int y_pos);
Score_Wrapper_Type * initialize_score_wrapper(
    Score_Wrapper_Type * score_wrapper,
    int x_position,
    int y_position,
    int * win_width,
    int * win_height
);

#endif