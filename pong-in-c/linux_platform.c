#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<X11/keysym.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<unistd.h>
#include"game.c"

#ifndef TRUE_FALSE 
#define TRUE 1
#define FALSE 0
#endif

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

/*Function prototypes*/
static int create_window(void);
static void close_window(void);
extern int delay(int i);
extern int time_diff(void);

/*Global variables*/
static int width;
static int height;
static Display *display;
static int screen;
static Window win;
static XEvent event;

int create_window(void) {
    char *window_name = "Pong";
    char *icon_name = "window";
    display = XOpenDisplay(NULL);

    Window rootwin;
    width = WINDOW_WIDTH;
    height = WINDOW_HEIGHT;
    static XSizeHints size_hints;

    if(display == NULL) {
        fprintf(stderr, "Verbindung zum X-Server fehlgeschlagen.\n");
        exit(EXIT_FAILURE);
    }

    screen = DefaultScreen(display);
    rootwin = RootWindow(display, screen);
    
    win = XCreateSimpleWindow(display, rootwin,
            100, 10, width, height,
            5, 
            BlackPixel(display, screen), 
            WhitePixel(display, screen));

    size_hints.flags = PSize | PMinSize | PMaxSize;
    size_hints.min_width = width;
    size_hints.max_width = width;
    size_hints.min_height = height;
    size_hints.max_height = height;
    XSetStandardProperties(display, win, window_name, icon_name, None, 0, 0, &size_hints);

    XSelectInput(display, win, 
    ButtonPressMask | 
    ButtonReleaseMask | 
    KeyPressMask | 
    KeyReleaseMask |
    EnterWindowMask |
    LeaveWindowMask |
    ExposureMask);

    XMapWindow(display, win);

    return 1;
}

void close_window(void) {
    XDestroyWindow(display, win);
    XCloseDisplay(display);
}

 int main(int argc, char ** argv) {
    int quit = FALSE;

    create_window();

    simulate_game(&win, display, &screen, &width, &height);

    close_window();

    return EXIT_SUCCESS;
}