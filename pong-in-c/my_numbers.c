#include "my_numbers.h"
#include<stdio.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>

#define NUMBERS 10

void draw_zero(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    int horizontal_y_pos[] = {y_position, y_position+60};
    
    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, x_position, horizontal_y_pos[i], 30, 10);
    }

    int vertical_x_pos[] = {x_position, x_position+20};

    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, vertical_x_pos[i], y_position+10, 10, 50);
    }
}

void draw_one(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    XFillRectangle(display, *win, *gc, x_position+20, y_position, 10, 70);
}

void draw_two(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    int horizontal_y_pos[] = {y_position, y_position+30, y_position+60};

    for (int i = 0; i <= 2; i++)
    {
        XFillRectangle(display, *win, *gc, x_position, horizontal_y_pos[i], 30, 10);
    }

    int vertical_x_pos[] = {x_position+20, x_position};
    int vertical_y_pos[] = {y_position+10, y_position+40};

    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, vertical_x_pos[i], vertical_y_pos[i], 10, 20);
    }
}

void draw_three(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    for (int i = y_position; i <= y_position+60; i += 30)
    {
        XFillRectangle(display, *win, *gc, x_position, i, 30, 10);
    }

    XFillRectangle(display, *win, *gc, x_position+20, y_position+10, 10, 50);
}

void draw_four(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    XFillRectangle(display, *win, *gc, x_position, y_position, 10, 40);
    XFillRectangle(display, *win, *gc, x_position+20, y_position, 10, 70);
    XFillRectangle(display, *win, *gc, x_position+10, y_position+30, 10, 10);
}

void draw_five(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    /*
        Need idea how to mirror draw_two()
    */
    int horizontal_y_pos[] = {y_position, y_position+30, y_position+60};

    for (int i = 0; i <= 2; i++)
    {
        XFillRectangle(display, *win, *gc, x_position, horizontal_y_pos[i], 30, 10);
    }

    int vertical_x_pos[] = {x_position+20, x_position};
    int vertical_y_pos[] = {y_position+40, y_position+10};

    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, vertical_x_pos[i], vertical_y_pos[i], 10, 20);
    }
}

void draw_six(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    int horizontal_x_pos[] = {x_position, x_position+10, x_position+10};
    int horizontal_y_pos[] = {y_position, y_position+30, y_position+60};
    int width[] = {30, 10, 10};

    for (int i = 0; i <= 2; i++)
    {
        XFillRectangle(display, *win, *gc, horizontal_x_pos[i], horizontal_y_pos[i], width[i], 10);
    }
    
    int vertical_x_pos[] = {x_position, x_position+20};
    int vertical_y_pos[] = {y_position+10, y_position+30};
    int height[] = {60, 40};

    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, vertical_x_pos[i], vertical_y_pos[i], 10, height[i]);
    }
}

void draw_seven(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    XFillRectangle(display, *win, *gc, x_position, y_position, 30, 10);
    XFillRectangle(display, *win, *gc, x_position+20, y_position+10, 10, 60);
}

void draw_eight(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    for (int i = y_position; i <= y_position+60; i += 30)
    {
        XFillRectangle(display, *win, *gc, x_position+10, i, 10, 10);
    }
    
    for (int i = x_position; i <= x_position+20; i += 20) 
    {
        XFillRectangle(display, *win, *gc, i, y_position, 10, 70);
    }
}

void draw_nine(Display * display, Window * win, GC * gc, int x_position, int y_position) {
    int horizontal_y_pos[] = {y_position, y_position+30};

    for (int i = 0; i <= 1; i++)
    {
        XFillRectangle(display, *win, *gc, x_position, horizontal_y_pos[i], 30, 10);
    }
    
    int vertical_x_pos[] = {x_position, x_position+20, x_position+20};
    int vertical_y_pos[] = {y_position+10, y_position+10, y_position+40};
    int height[] = {20, 20, 30};

    for (int i = 0; i <= 2; i++)
    {
        XFillRectangle(display, *win, *gc, vertical_x_pos[i], vertical_y_pos[i], 10, height[i]);
    }
}

void draw_number(Display * display, Window * win, GC * gc, int x_position, int y_position, int number) {
    
    void (*draw_function_ptrs[NUMBERS])(Display*, Window*, GC*, int, int) = 
        {draw_zero, draw_one, draw_two, draw_three, draw_four, draw_five, draw_six, draw_seven, draw_eight, draw_nine};

    draw_function_ptrs[number](display, win, gc, x_position, y_position);
}