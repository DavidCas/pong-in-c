#include "paddle.h"
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<stdio.h>
#include<stdlib.h>

Paddle * initialize_paddle(int x_position, int y_position, int width, int height) {

    Paddle * instanciated_paddle = malloc(sizeof(Paddle));

    if (instanciated_paddle!=NULL)
    {
        instanciated_paddle->x_position = x_position;
        instanciated_paddle->y_position = y_position;
        instanciated_paddle->width = width;
        instanciated_paddle->height = height;
    }

    return instanciated_paddle;
}

void move_paddle_up(Paddle * paddle_to_move) {

    if (paddle_to_move->y_position <= 0)
        return;
    
    paddle_to_move -> y_position-=20;
}

void move_paddle_down(Paddle * paddle_to_move, int * win_height) {

    if(paddle_to_move->y_position >= (*win_height - paddle_to_move->height))
        return;

    paddle_to_move -> y_position+=20;
}

void draw_paddle(Display *display, Window *win, GC *gc, Paddle *paddle_to_draw) {
    XFillRectangle(display, *win, *gc, 
    paddle_to_draw -> x_position, 
    paddle_to_draw -> y_position, 
    paddle_to_draw -> width, 
    paddle_to_draw -> height);
}
