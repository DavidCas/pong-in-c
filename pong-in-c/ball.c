#include "ball.h"
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<stdbool.h>
#include<stdio.h>
#include "paddle.h"
#include "environment.h"

Ball * initialize_ball( 
    int x_position, 
    int y_position, 
    int width, 
    int height, 
    float * frametime) 
{
    Ball * instanciated_ball = malloc(sizeof(Ball));

    if (instanciated_ball != NULL)
    {
        instanciated_ball->x_position = x_position;
        instanciated_ball->y_position = y_position;
        instanciated_ball->x_pos_internal = (float) x_position;
        instanciated_ball->y_pos_internal = (float) y_position;
        instanciated_ball->width = width;
        instanciated_ball->height = height;
        instanciated_ball->velocity_x = 7/(*frametime);
        instanciated_ball->velocity_y = 7/(*frametime);
    }

    return instanciated_ball;
}

/**
 * Returns 1, if integer position changed.
 * */
int move_ball(
    Ball * ball_to_move, 
    float * frametime,
    int * win_width, 
    Paddle * left_paddle, 
    Paddle * right_paddle, 
    Border * top_border,
    Border * bottom_border) {

    int previous_x_position = ball_to_move->x_position;
    int previous_y_position = ball_to_move->y_position;

    ball_to_move->velocity_x *= (is_collision_with_paddle(ball_to_move, left_paddle, right_paddle) ? -1 : 1);
    ball_to_move->velocity_y *= (is_collision_with_border(ball_to_move, top_border, bottom_border) ? -1 : 1);
    ball_to_move->velocity_x *= (is_ball_out_of_bounds(ball_to_move, *win_width) ? -1 : 1);

/*
    if (is_collision_with_paddle(ball_to_move, left_paddle, right_paddle))
    {
        printf("Is collision with paddle.\n");
    }
    if (is_collision_with_border(ball_to_move, top_border, bottom_border)) {
        printf("Is collision with border.\n");
    }*/
    

    ball_to_move->x_pos_internal += (ball_to_move->velocity_x * (*frametime));
    ball_to_move->y_pos_internal += (ball_to_move->velocity_y * (*frametime));

    ball_to_move->x_position = (int)ball_to_move->x_pos_internal;
    ball_to_move->y_position = (int)ball_to_move->y_pos_internal;

    return (
        (previous_x_position == ball_to_move->x_position) &&
        (previous_y_position == ball_to_move->y_position)) ? 
        0 : 1;
}

void draw_ball(Display *display, Window *win, GC *gc, Ball *ball) {
    XFillRectangle(display, *win, *gc, 
    ball -> x_position, 
    ball -> y_position, 
    ball -> width, 
    ball -> height);
}

bool is_collision_with_paddle(Ball *ball, Paddle *left_paddle, Paddle *right_paddle) {
    bool is_collision_with_left_paddle = 
        left_paddle->y_position < ball->y_position &&
        left_paddle->y_position + left_paddle->height > ball->y_position &&
        left_paddle->x_position + left_paddle->width > ball->x_position;

    bool is_collission_with_right_paddle = 
        right_paddle->y_position < ball->y_position &&
        right_paddle->y_position + right_paddle->height > ball->y_position &&
        right_paddle->x_position <= ball->x_position+ball->width;

    return is_collision_with_left_paddle || is_collission_with_right_paddle;
}

void change_ball_position(Ball *ball, int * new_x_pos, int * new_y_pos) {
    ball->x_position = *new_x_pos;
    ball->y_position = *new_y_pos;
    ball->x_pos_internal = ball->x_position;
    ball->y_pos_internal = ball->y_position; 
}

bool is_collision_with_border(Ball *ball, Border *top_border, Border *bottom_border) {
    return 
        top_border->y_position+top_border->height >= ball->y_position ||
        bottom_border->y_position<=ball->y_position+ball->height;
}

bool is_ball_out_of_bounds(Ball *ball, int win_width) {
    return
        ball->x_position <= 0 || 
        ball->x_position + ball->width >= win_width ? 
        true : false;
}

bool has_ball_moved(
    Ball * ball_to_check, 
    float * frametime, 
    int * win_half_width, 
    Paddle * left_paddle, 
    Paddle * right_paddle, 
    Border * top_border,
    Border * bottom_border) {
    return  (move_ball(
            ball_to_check, 
            frametime, 
            win_half_width, 
            left_paddle,
            right_paddle,
            top_border,
            bottom_border) == 1) ? true : false;
}
