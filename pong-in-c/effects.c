#include "effects.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

double get_distance_from_explosion_center_to_particle(struct Explosion * explosion);

struct Explosion * instanciate_Explosion(int * x_position, int * y_position, int number_of_particles, int max_radius) {
    struct Explosion * explosion = malloc(sizeof(struct Explosion));

    explosion->explosion_expansion = 0.0;
    explosion->x_position = *x_position;
    explosion->y_position = *y_position;
    explosion->max_radius = max_radius;
    explosion->number_of_particles = number_of_particles;
    explosion->particles = (struct Explosion_Particle*)calloc
        (explosion->number_of_particles, sizeof(struct Explosion_Particle));

    instanciate_particles(explosion->particles, explosion);
    explosion->is_in_progress = true;

    return explosion; 
}

void instanciate_particles(struct Explosion_Particle * particles, struct Explosion * explosion) {
    //Use idea of colision checking, to reverse velocity
    for (int i = 0; i < explosion->number_of_particles; i++) {
        (particles+i)->x_position = explosion->x_position;
        (particles+i)->y_position = explosion->y_position;
    }
}

bool is_explosion_in_progress(struct Explosion * explosion) {

    if(explosion->is_in_progress) {
        expand(explosion);
        return explosion->is_in_progress;
    }
    
    return explosion->is_in_progress;
}

void expand(struct Explosion * explosion) {
    explosion->explosion_expansion += 1;

    int x_shift_sign = 1;
    int y_shift_sign = 1;

    for (int i = 0; i < explosion->number_of_particles; i++) {
        (explosion->particles+i)->x_position += explosion->explosion_expansion;
        (explosion->particles+i)->y_position += explosion->explosion_expansion;
    }

    explosion->is_in_progress = (explosion->particles->x_position != explosion->x_position + explosion->max_radius);
}

void explode(struct Explosion * explosion) {
    explosion->is_in_progress = true;
    expand(explosion);
}

/**
 * This is not the way I want it to work like that. Like, I can't
 * believe I even implemented this solution, like holy f... this
 * is so disgusting. But I wanna have that animation really,
 * really bad. I'll implement my PROPER idea later, when I 
 * figured out the mathematical algorithm to describe the pattern.
 * Shame on me tho...
 * */
void expand_explosion_the_noob_way(struct Explosion * explosion) {

    explosion->explosion_expansion += 1.0;

    /*Find an algorithm, that describes a pattern inside this numbers
    for each number of given particles.*/
    float x_factors[] = {1.0f, 1.0/5.0, 1.0, -1.0/5.0, -2.0/5.0, -3.0/5.0, -2.0/5.0, -1.0/5.0};
    float y_factors[] = {1.0, -1.0/5.0, -2.0/5.0, -3.0/5.0, -2.0/5.0, -1.0/5.0, 1.0, 1.0/5.0};

    for (int i = 0; i <= 8; i++) {
        (explosion->particles+i)->x_position += explosion->explosion_expansion * x_factors[i];
        (explosion->particles+i)->y_position += explosion->explosion_expansion * y_factors[i];
    }

    //printf("%f\n", get_distance_explosion_center_and_particle(explosion));

    if (get_distance_from_explosion_center_to_particle(explosion) >= explosion->max_radius) {
        explosion->is_in_progress = false;
        return;    
    }
    explosion->is_in_progress = true;
}

void draw_explosion(Display * display, Window * win, GC * gc, struct Explosion * explosion) {
    for (int i = 0; i <= explosion->number_of_particles; i++) {
        XFillRectangle(display, *win, *gc, 
        (explosion->particles+i)->x_position, 
        (explosion->particles+i)->y_position, 10, 10);
    }
}

double get_distance_from_explosion_center_to_particle(struct Explosion * explosion) {
    float x_pos = (float)explosion->x_position;
    float y_pos = (float)explosion->y_position;
    float x_distance = x_pos - (explosion->particles+1)->x_position;
    float y_distance = y_pos - (explosion->particles+1)->y_position;

    //Great Pythagoras, but a more efficient resolution will be researched.
    return sqrt(pow(x_distance, 2) + pow(y_distance, 2));
}

struct Y_Shake_Animation * instanciate_Shake(float max_amplitude) {
    struct Shake_Animation * shake = (struct Shake_Animation*) malloc(sizeof(struct Shake_Animation)); 
    shake->amplitude = max_amplitude;
    shake->is_in_progress = false;
}

float shake_y_pos(struct Shake_Animation * shake, float y_position) {
    if (!shake->is_in_progress) {
        shake->is_in_progress = true;
        shake->next_max_amplitude = y_position + shake->amplitude;
        shake->amplitude_end_value = shake->amplitude/100.0f;
        shake->old_y_pos = y_position;
        float new_y_pos = y_position + (shake->amplitude/10.0f);
        return new_y_pos;
    }

    if (y_position >= shake->next_max_amplitude) {
        shake->amplitude *= -1;
        shake->next_max_amplitude = y_position - (2.0f * shake->amplitude);
    }

    if (shake->amplitude <= shake->amplitude_end_value) {
        shake->is_in_progress = false;
        return shake->old_y_pos;
    }

    return y_position + (shake->amplitude/10.0f);
}
